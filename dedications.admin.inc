<?php
/**
 * @file
 * Content administration and module settings UI.
 */

/**
 * Define the settings form.
 */
function dedications_admin_settings() {
  $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
  );
  $form['settings']['dedication_nodetypes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Choose the content types which you want to send as dedication'),
      '#options' => node_get_types('names'),
      '#default_value' => variable_get('dedication_nodetypes', array()),
  );
  $form['settings']['dedication_cron'] = array(
      '#type' => 'textfield',
      '#title' => t('Days to keep dedications'),
      '#description' => t('Dedication older than the value entered here will automatically be deleted when cron runs. To prevent dedications deletion, set this value to 0.'),
      '#size' => '2',
      '#default_value' => variable_get('dedication_cron', '0'),
  );
  $form['settings']['dedication_max_count'] = array(
      '#type' => 'textfield',
      '#title' => 'Maximum number of emails allowed to send at a time',
      '#description' => 'The default value is 100. It is recommended to use a low number so mails sent from your server are not treated as spam. If "time out" errors or "white screens" occur then reduce the number.',
      '#size' => '5',
      '#default_value' => variable_get('dedications_max_count', '100'),
  );
  $form['settings']['dedications_thank_you_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to thank you page'),
      '#default_value' => variable_get('dedications_thank_you_page', 'dedication/thanks'), '#description' => t('Enter the path to the thank you page where users should be redirected to after sending an dedications.  For example, <em>dedication/thanks</em>, <em>node/123</em>.  Leave blank for no redirection.'),
  );
  $form['settings']['dedications_hide_send_view'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the send dedications form when viewing the dedications message.'),
      '#default_value' => variable_get('dedications_hide_send_view', 0),
  );
  $form['settings']['dedications_require_name'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require the sender to provide their name.'),
      '#default_value' => variable_get('dedications_require_name', 0),
  );
  $form['settings']['dedications_require_message'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require the sender to enter a personal message.'),
      '#default_value' => variable_get('dedications_require_message', 0),
  );
  $form['settings']['dedications_fill_in_name_email'] = array(
      '#type' => 'checkbox',
      '#title' => t('Automatically fill in sender name and email for logged in users.'),
      '#default_value' => variable_get('dedications_fill_in_name_email', 0),
  );
  $form['settings']['fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Message fieldset settings'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['settings']['fieldset']['dedication_is_fieldset'] = array(
      '#type' => 'checkbox',
      '#title' => t('Put the dedications form in a fieldset?'),
      '#default_value' => variable_get('dedication_is_fieldset', 1),
  );
  $form['settings']['fieldset']['dedication_is_fieldset_collapsible'] = array(
      '#type' => 'checkbox',
      '#title' => t('Is the fieldset collapsible?'),
      '#default_value' => variable_get('dedication_is_fieldset_collapsible', 1),
  );
  $form['settings']['fieldset']['dedication_is_fieldset_collapsed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Is the fieldset collapsed?'),
      '#default_value' => variable_get('dedication_is_fieldset_collapsed', 0),
  );

  $form['letter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dedication delivery message'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['letter']['dedication_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('dedication_subject', 'Dedication from %site'),
      '#size' => 70,
      '#maxlength' => 70,
      '#description' => t('Customize the subject for dedication'),
  );
  $form['letter']['dedication_letter'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get('dedication_letter', _dedications_letter()),
      '#cols' => 70,
      '#rows' => 5,
      '#description' => t('This text is the body of the email that the dedication recipient will see. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site email address, %card_url = the URL for the dedication, %sender = sender name, %sender_email = sender email, %recipient = recipient email'),
  );

  $form['copy'] = array(
      '#type' => 'fieldset',
      '#title' => t("Sender's dedication copy message"),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['copy']['dedication_copy_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('dedication_copy_subject', 'Copy of your dedication'),
      '#size' => 70,
      '#maxlength' => 70,
      '#description' => t('Customise the email sent to the sender as copy of the dedication.'),
  );
  $form['copy']['dedication_copy'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get('dedication_copy', _dedications_copy()),
      '#cols' => 70,
      '#rows' => 5,
      '#description' => t('This text is the body of the email to notify the sender that his dedication has been picked up. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site email address, %card_url = the URL for the dedication'),
  );

  $form['notify'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dedication collection notification message'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['notify']['dedication_notify_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable option for dedication collection notification emails'),
      '#default_value' => variable_get('dedication_notify_enabled', TRUE),
  );
  $form['notify']['dedication_notify_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('dedication_notify_subject', 'Your dedication has been just picked up'),
      '#size' => 70,
      '#maxlength' => 70,
      '#description' => t('Customize the subject for dedication'),
  );
  $form['notify']['dedication_notify'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get('dedication_notify', _dedications_notify()),
      '#cols' => 70,
      '#rows' => 5,
      '#description' => t('This text is the body of the email to notify the sender that the dedication has been picked up. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site email address, %card_url = the URL for the dedication, %sender = sender name, %recipient = recipient email'),
  );

  $form['array_filter'] = array('#type' => 'hidden');

  return system_settings_form($form);
}

