<?php

// dedications.module,v 1.1.4.20 2009/12/22 19:27:11 karst Exp $

/**
 * @file
 * This module can be used to send any node as an dedication.
 */

/**
 * Implementation of hook_perm().
 */
function dedications_perm() {
  return array('view dedications', 'send dedications', 'send own content as dedication');
}

/**
 * Implementation of hook_access().
 *
 * Used for checking if user has the rights to send dedications
 */
function dedications_form_access($uid) {
  return user_access('send dedications') || (user_access('send own content as dedications') && ($GLOBALS['user']->uid == $uid));
}

/**
 * Implementation of hook_menu().
 */
function dedications_menu() {

  $items['admin/settings/dedications'] = array(
      'title' => t('Dedications settings'),
      'description' => t('Change how dedication behave.'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('dedications_admin'),
      'access arguments' => array('administer site configuration'),
  );
  $items['dedication/view'] = array(
      'page callback' => 'dedications_view',
      'type' => MENU_CALLBACK,
      'access arguments' => array('view dedications'),
  );
  $items['dedication/thanks'] = array(
      'page callback' => 'dedications_thanks',
      'type' => MENU_CALLBACK,
      'access arguments' => array('view dedications'),
  );

  $items['admin/build/dedications/settings'] = array(
   'title' => 'Dedications settings',
   'page callback' => 'drupal_get_form',
   'page arguments' => array('dedicaations_admin_settings'),
   'access arguments' => array('administer dedications'),
   'type' => MENU_LOCAL_TASK,
   'file' => 'dedications.admin.inc',
 );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function dedications_views_api() {
  return array(
  'api' => 2,
  'path' => drupal_get_path('module', 'dedications'),
  );
}

/**
 * Define the settings form.
 */
function dedications_admin() {
  $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
  );
  $form['settings']['dedication_nodetypes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Choose the content types which you want to send as dedication'),
      '#options' => node_get_types('names'),
      '#default_value' => variable_get('dedication_nodetypes', array()),
  );
  $form['settings']['dedication_cron'] = array(
      '#type' => 'textfield',
      '#title' => t('Days to keep dedications'),
      '#description' => t('Dedication older than the value entered here will automatically be deleted when cron runs. To prevent dedications deletion, set this value to 0.'),
      '#size' => '2',
      '#default_value' => variable_get('dedication_cron', '0'),
  );
  $form['settings']['dedication_max_count'] = array(
      '#type' => 'textfield',
      '#title' => 'Maximum number of emails allowed to send at a time',
      '#description' => 'The default value is 100. It is recommended to use a low number so mails sent from your server are not treated as spam. If "time out" errors or "white screens" occur then reduce the number.',
      '#size' => '5',
      '#default_value' => variable_get('dedications_max_count', '100'),
  );
  $form['settings']['dedications_thank_you_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to thank you page'),
      '#default_value' => variable_get('dedications_thank_you_page', 'dedication/thanks'), '#description' => t('Enter the path to the thank you page where users should be redirected to after sending an dedications.  For example, <em>dedication/thanks</em>, <em>node/123</em>.  Leave blank for no redirection.'),
  );
  $form['settings']['dedications_hide_send_view'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the send dedications form when viewing the dedications message.'),
      '#default_value' => variable_get('dedications_hide_send_view', 0),
  );
  $form['settings']['dedications_require_name'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require the sender to provide their name.'),
      '#default_value' => variable_get('dedications_require_name', 0),
  );
  $form['settings']['dedications_require_message'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require the sender to enter a personal message.'),
      '#default_value' => variable_get('dedications_require_message', 0),
  );
  $form['settings']['dedications_fill_in_name_email'] = array(
      '#type' => 'checkbox',
      '#title' => t('Automatically fill in sender name and email for logged in users.'),
      '#default_value' => variable_get('dedications_fill_in_name_email', 0),
  );
  $form['settings']['fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Message fieldset settings'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['settings']['fieldset']['dedication_is_fieldset'] = array(
      '#type' => 'checkbox',
      '#title' => t('Put the dedications form in a fieldset?'),
      '#default_value' => variable_get('dedication_is_fieldset', 1),
  );
  $form['settings']['fieldset']['dedication_is_fieldset_collapsible'] = array(
      '#type' => 'checkbox',
      '#title' => t('Is the fieldset collapsible?'),
      '#default_value' => variable_get('dedication_is_fieldset_collapsible', 1),
  );
  $form['settings']['fieldset']['dedication_is_fieldset_collapsed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Is the fieldset collapsed?'),
      '#default_value' => variable_get('dedication_is_fieldset_collapsed', 0),
  );

  $form['letter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dedication delivery message'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['letter']['dedication_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('dedication_subject', 'Dedication from %site'),
      '#size' => 70,
      '#maxlength' => 70,
      '#description' => t('Customize the subject for dedication'),
  );
  $form['letter']['dedication_letter'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get('dedication_letter', _dedications_letter()),
      '#cols' => 70,
      '#rows' => 5,
      '#description' => t('This text is the body of the email that the dedication recipient will see. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site email address, %card_url = the URL for the dedication, %sender = sender name, %sender_email = sender email, %recipient = recipient email'),
  );

  $form['copy'] = array(
      '#type' => 'fieldset',
      '#title' => t("Sender's dedication copy message"),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['copy']['dedication_copy_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('dedication_copy_subject', 'Copy of your dedication'),
      '#size' => 70,
      '#maxlength' => 70,
      '#description' => t('Customise the email sent to the sender as copy of the dedication.'),
  );
  $form['copy']['dedication_copy'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get('dedication_copy', _dedications_copy()),
      '#cols' => 70,
      '#rows' => 5,
      '#description' => t('This text is the body of the email to notify the sender that his dedication has been picked up. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site email address, %card_url = the URL for the dedication'),
  );

  $form['notify'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dedication collection notification message'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
  );
  $form['notify']['dedication_notify_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable option for dedication collection notification emails'),
      '#default_value' => variable_get('dedication_notify_enabled', TRUE),
  );
  $form['notify']['dedication_notify_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('dedication_notify_subject', 'Your dedication has been just picked up'),
      '#size' => 70,
      '#maxlength' => 70,
      '#description' => t('Customize the subject for dedication'),
  );
  $form['notify']['dedication_notify'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get('dedication_notify', _dedications_notify()),
      '#cols' => 70,
      '#rows' => 5,
      '#description' => t('This text is the body of the email to notify the sender that the dedication has been picked up. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site email address, %card_url = the URL for the dedication, %sender = sender name, %recipient = recipient email'),
  );

  $form['array_filter'] = array('#type' => 'hidden');

  return system_settings_form($form);
}

/**
 * Implementation of hook_cron().
 */
function dedications_cron() {
// Convert cron days into timestamp.
  $dedication_cron = variable_get('dedication_cron', '0') * 86400;
  // Delete dedications older than setting.
  if (!empty($dedication_cron)) {
    db_query('DELETE FROM {dedications} WHERE send_time < %d', time() - $dedication_cron);
  }

  //Scheduled
  $unixtime_today = mktime( 0, 0, 0, date("n"), date("j"), date("Y"));
  $query = "SELECT * FROM {dedications} WHERE send_time = %d AND send = 'n'";
  $result = db_query($query, $unixtime_today);

  while ($dedication = db_fetch_object($result)) {
    dedications_send_card_scheduled( $dedication );
  }

}

/**
 * Function for making letter to send.
 */
function _dedications_letter() {
  $output = t("Hi,\n\n%sender made an dedication for you.\nAt any time you may see your card by clicking this link:\n\n%card_url\n\n(if your email client doesn't allow you to click on the site link,\nthen just copy and paste the URL into your browser)\n\nadmin");
  return $output;
}

/**
 * Implementation of hook_nodeapi().
 */
function dedications_nodeapi(&$node, $op, $teaser, $page) {
  if ( $op === 'view' && dedications_types($node->type)) {
  // Add our form as a content item.
    if ($teaser === FALSE) {
      $_hide = variable_get('dedications_hide_send_view', 0);
      if (!empty($node->content['dedication_view']) && $_hide == 1  ) {
        return;
      }
      $node->content['dedication_form'] = array(
          '#value' => drupal_get_form('dedications_form', $node),
          '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'dedications_form') : 100,
      );

    }

  }

}

function dedications_types($type) {
  if (!in_array($type, variable_get('dedication_nodetypes', array()))) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_content_extra_fields().
 */
function dedications_content_extra_fields($type) {
  if (!dedications_types($type)) {
    return array();
  }
  else {
    $extras['dedication_form'] = array(
        'label' => t('Dedication form'),
        'description' => t('Displays the dedication form'),
        'weight' => 100,
    );
    return $extras;
  }
}

/**
 * Function to define the dedication form.
 */
function dedications_form($form_state, &$node) {
  drupal_add_css(drupal_get_path('module', 'dedications') .'/dedications.css');
  drupal_add_css(drupal_get_path('module', 'dedications') .'/ui.theme.css');
  drupal_add_css(drupal_get_path('module', 'dedications') .'/ui.datepicker.css');
  drupal_add_js(drupal_get_path('module', 'dedications') .'/ui.core.js');
  drupal_add_js(drupal_get_path('module', 'dedications') .'/ui.datepicker.js');
  drupal_add_js("
	$(function() {
		$(\"#datepicker\").datepicker({minDate: 0, maxDate: '+6M +10D'});
	});
  ", "inline");

  if (!dedications_form_access($node->uid)) {
    return $form;
  }

  // Sender's name.
  $form['name'] = array(
      '#title' => t(' '),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 30,
      '#default_value' => variable_get('dedication_fill_in_name_email', 0) && user_is_logged_in() ? $GLOBALS['user']->name : '',
      '#prefix' => '<div id="dedication_top">&nbsp;</div><div id="dedication_centro"><div id="dedication_contenedor"><div class="from_name_left">', 
      '#suffix' => '</div>'
  );
  // Sender's email.
  $form['from_email'] = array(
      '#title' => t(' '),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 30,
      '#default_value' => variable_get('dedication_fill_in_name_email', 0) && user_is_logged_in() ? $GLOBALS['user']->mail : '',
      '#prefix' => '<div class="from_name_right">',
      '#suffix' => '</div><br class="clear"></div></div><div id="dedication_bottom">&nbsp;</div>'
  );

  $form['to_email'] = array(
      '#title' => t(' '),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => '',
      '#description' => t('You may enter multiple emails'),
      '#required' => TRUE,
      '#prefix' => '<div id="dedication_top">&nbsp;</div><div id="dedication_centro"><div id="dedication_contenedor">',
      '#suffix' => '<br class="clear"></div></div><div id="dedication_bottom">&nbsp;</div>'
  );

  // Display textarea to type message.
  $form['message'] = array(
      '#title' => t(' '),
      '#type' => 'textarea',
      '#description' => t('Whatever you type here will be attached to the e-card'),
      '#required' => variable_get('dedication_require_message', 0),
      '#prefix' => '<div id="dedication_top">&nbsp;</div><div id="dedication_centro"><div id="dedication_contenedor">',
      '#suffix' => '<br class="clear"></div></div><div id="dedication_bottom">&nbsp;</div>',
      '#weight' => -10,
  );

  // Filter format for the text.
  //$form['filter'] = filter_form();

   //Copy of dedication
   $form['copy_content'] = array(
        '#type' => 'checkbox',
        '#title' => t('Recieve a copy of this content'),
        '#default_value' => 0,
        '#prefix' => '<div id="dedication_top">&nbsp;</div><div id="dedication_centro"><div id="dedication_contenedor">'
   );

  // Notification on pick up.
  if (variable_get('dedication_notify_enabled', TRUE)) {
    $form['notify'] = array(
        '#type' => 'checkbox',
        '#title' => t('Notify me when the dedication is picked up'),
        '#default_value' => 0
    );
  }

   $hoy = date("m") . "/" . date('d') . "/" . date("Y");

   //Send time
   $form['scheduled'] = array(
     '#id' => 'datepicker',
     '#type' => 'textfield',
     '#title' => t('Date Posted'),
     '#attributes' => array('readonly' => 'readonly'),
     '#default_value' => $hoy
   );

  $form['nid'] = array(
      '#type' => 'value',
      '#value' => $node->nid
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send this dedication'),
      '#weight' => 100,
      '#suffix' => '<br class="clear"></div></div><div id="dedication_bottom">&nbsp;</div>'
  );

  /*
  if (variable_get('dedication_is_fieldset', 1)) {
    $form_fieldset['block'] = array(
        '#type' => 'fieldset',
        '#title' => t('Send an dedication to your friends'),
        '#collapsible' => variable_get('dedication_is_fieldset_collapsible', 1),
        '#collapsed' => variable_get('dedication_is_fieldset_collapsed', 0),
        $form
    );
    $form = $form_fieldset;
  }*/
  return $form;
}

/**
 * Validation function for email syntax.
 */
function dedications_validate_emails($form_id, &$form_state) {
  if (valid_email_address($form_state['values']['from_email']) != TRUE) {
    form_set_error('from_email', t('Your email address is invalid'));
  }

  $valid_emails = $failed_emails = array();
  $emails = array_unique(split("[,\n\r]", $form_state['values']['to_email']));
  foreach ($emails as $email) {
    $email = trim($email);
    if ($email) {
      if (valid_email_address($email)) {
        $valid_emails[] = $email;
      }
      else {
        $failed_emails[] = $email;
      }
    }
  }

  if (count($failed_emails)) {
    form_set_error('to_email', t('This email has an error "@email"', array('@email' => $failed_emails[0])));
  }

  return $valid_emails;
}

/**
 * Validation function for the form if call email_import of validate emails on respective steps.
 */
function dedications_form_validate($form_id, &$form_state) {
  $emails = dedications_validate_emails($form_id, $form_state);
  $count = count($emails);
  $max_count = variable_get('dedication_max_count', 1000);
  if ($count > $max_count) {
    form_set_error('to_email', t('You are not allowed to send to more than !count emails, please delete !diff email address and send the e-card', array('!count' => $max_count, '!diff' => $count - $max_count)));
  }
  form_set_value(array('#parents' => array('valid_emails')), $emails, $form_state);
}

/**
 * Submit handler for dedications_form.
 */
function dedications_form_submit($form_id, &$form_state) {
// Set up the dedication record.
  $_aux_date = split("/", $form_state['values']['scheduled']);
  $unixtime_today = mktime(0, 0, 0, $_aux_date[0], $_aux_date[1], $_aux_date[2]);

  $dedication = new stdClass;
  $dedication->nid = $form_state['values']['nid'];
  $dedication->sender_name = $form_state['values']['name'];
  $dedication->sender_email = $form_state['values']['from_email'];
  $dedication->valid_names = $form_state['values']['valid_names'];
  $dedication->valid_emails = $form_state['values']['valid_emails'];
  $dedication->message = $form_state['values']['message'];
  $dedication->send_time = $unixtime_today;
  $dedication->copy_dedication = $form_state['values']['copy_content'];
  $dedication->send = 'y';
  $dedication->notify = $form_state['values']['notify'];
  $dedication->format = $form_state['values']['format'];

  // Send the dedications, including copies.
  dedications_send_card($dedication);

  // Display a message.
  drupal_set_message(format_plural(count($form_state['values']['valid_emails']), 'Your dedication has been sent.', 'Your dedications have been sent.'));

  // Redirect to thank you page if necessary.
  $thank_you_page = variable_get('dedication_thank_you_page', 'dedication/thanks');
  if (!empty($thank_you_page)) {
    $form_state['redirect'] = $thank_you_page;
  }
}

function dedications_send_card_scheduled( $dedication ) {
  global $base_url;
  global $language;

  // Set the base variable from the clean url value.
  if (variable_get('clean_url', 0)) {
    $base = $base_url .'/';
  }
  else {
    $base = $base_url .'/index.php?q=';
  }

  $params['card'] = $base .'dedication/view/'. $cardid;
  $params['sender'] = $dedication->sender_name;
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  $headers = array(
    'Reply-to' => $from,
    'Return-path' => "<$from>",
    'Errors-to' => $from
  );
  $params['recipient'] = $email;
  $params['headers']   = $headers;
  drupal_mail('dedications', 'dedication-mail', $dedication->recp_mail, $language, $params, $from, TRUE);

  db_query("UPDATE {dedications} SET send='y' WHERE random='%s'", $dedication->random);
}

/**
 * Send dedications, including copies if necessary.
 */
function dedications_send_card($dedication) {
  global $base_url;
  global $language;

  // Set the base variable from the clean url value.
  if (variable_get('clean_url', 0)) {
    $base = $base_url .'/';
  }
  else {
    $base = $base_url .'/index.php?q=';
  }

  // Iterate through each emails and send them and store a random number for
  // each.
  $aux_cont = 0;
  foreach ($dedication->valid_emails as $email) {
  // Append start value to random number to get cardid.
    $cardid =  md5(microtime() . $dedication->sender_email . $email);

    if (variable_get('dedication_notify_enabled', TRUE) && $dedication->notify == 1) {
    // Save record to the database.
      $dedication->notify = 'y';
    }
    else {
      $dedication->notify = 'n';
    }

    $dedication->random = $cardid;
    $dedication->recp_mail = $email;
    $dedication->recp_name = $dedication->valid_names[$aux_cont];

    $hoy = mktime( 0, 0, 0, date('m'), date('d'), date('Y') );

    if ( $hoy == $dedication->send_time ) {
      $params['card'] = $base .'dedication/view/'. $cardid;
      $params['sender'] = $dedication->sender_name;
      $from = variable_get('site_mail', ini_get('sendmail_from'));
      $headers = array(
        'Reply-to' => $from,
        'Return-path' => "<$from>",
        'Errors-to' => $from
      );
      $params['recipient'] = $email;
      $params['headers']   = $headers;
      drupal_mail('dedications', 'dedication-mail', $email, $language, $params, $from, TRUE);
    }
    else{
      $dedication->send = 'n';
    }

    dedications_save_card($dedication);
    $aux_cont++;
  }

  if ($dedication->copy_dedication == 1) {
  // Making a copy for you else if you click any other recp link it will mail
  // notification also there is some problem while we produce random.
    $dedication->send = 'y';
    $dedication->notify = 'n';
    $dedication->random = md5(microtime() . $dedication->sender_email);
    $dedication->recp_mail = $dedication->sender_email;
    dedications_save_card($dedication);

    $card_copy_url = $base .'dedication/view/'. $dedication->random;
    drupal_set_message(t('You can view the dedication at <a href="@url">here</a>.', array('@url' => $card_copy_url)));
    $from = variable_get('site_mail', ini_get('sendmail_from'));
    $headers = array(
      'Reply-to' => $from,
      'Return-path' => "<$from>",
      'Errors-to' => $from
    );
    $params['card_copy'] = $card_copy_url;
    $params['headers']   = $headers;
    drupal_mail('dedications', 'dedication-copy', $dedication->sender_email, $language, $params, $from, TRUE );
  }
}

/**
 * Function for making copy url letter.
 */
function _dedications_copy() {
  $output = t("Hi\n Here is the copy of the dedication for your reference \n!card_copy_url\nadmin");
  return $output;
}

/**
 * Function for making notification letter.
 */
function _dedications_notify() {
  $output = t("Hi,\n\n%recipient has picked your card today.\n here is the url to your card\n %card_url\n\n (if your email client doesn't allow you to click on the site link,\nthen just copy and paste the URL into your browser)\n\nadmin");
  return $output;
}

/**
 * Load a message and details based on the random string and let other modules
 * act on it.
 */
function dedications_load_card($random) {
  $sql = 'SELECT random, nid, message, sender_name, sender_email, recp_mail, notify, format FROM {dedications} AS ec WHERE ec.random = "%s"';
  $dedication = db_fetch_object(db_query($sql, $random));
  drupal_alter('dedications_load', $dedication);
  return $dedication;
}

/**
 * Save dedication to the database and let other modules act on it.
 */
function dedications_save_card($dedication) {
  drupal_alter('dedication_save', $dedication);
  drupal_write_record('dedications', $dedication);
  return $dedication;
}

/**
 * Save dedication to the database and let other modules act on it.
 * Same as save but with a primarykey "random" required for update.
 */
function dedications_update_card($dedication) {
  if (!$dedication->random) {
    return FALSE;
  }
  return dedications_save_card($dedication);
}

/**
 * Save dedication to the database and let other modules act on it.
 */
function dedications_delete_card($random) {
  return db_query('DELETE FROM {dedications} AS ec WHERE ec.random = "%s"', $random);
}

/**
 * Function to view the dedication , it also send an email to the sender to know the
 * card is picked up.
 */
function dedications_view($arg1 = NULL) {
  global $base_url;
  // Set the base variable from the clean url value.
  if (variable_get('clean_url', 0)) {
    $base = $base_url .'/';
  }
  else {
    $base = $base_url .'/index.php?q=';
  }
  if (isset($arg1)) {

    $dedication = dedications_load_card($arg1);

    if ($dedication != NULL) {
      $node = node_load($dedication->nid);
      $node->content['dedication_view'] = array(
          '#value' => theme('dedications_message', check_markup($dedication->message, $dedication->format)),
          '#weight' => 90,
      );

      $output = node_view($node, FALSE, TRUE, FALSE);

      // Sending email if notify == yes.
      if ($dedication->notify == 'y') {
        $card_url = $base .'dedication/view/'. $dedication->random;

        // Make message.
        $site_email = variable_get('site_mail', ini_get('sendmail_from'));
        $params['site'] = $site_email;
        $params['card_url'] = $card_url;
        $params['sender'] = $dedication->sender_name;
        $params['recipient'] = $dedication->recp_mail;
    $from = variable_get('site_mail', ini_get('sendmail_from'));
    $headers = array(
      'Reply-to' => $from,
      'Return-path' => "<$from>",
      'Errors-to' => $from
    );
        $params['headers'] = $headers;
        drupal_mail('dedications', 'dedication-notify', $dedications->sender_email, NULL, $params, $from, TRUE);
        db_query("UPDATE {dedications} SET notify='n' WHERE random='%s'", $dedication->random);
      }
    }
    else {
      return t('Your dedication id is not valid either it may be an error or it expired.');
    }
    return $output;
  }
  else {
    drupal_goto();
  }
}

/**
 * Implementation hook_mail().
 */
function dedications_mail($key, &$message, $params) {
  global $base_url;
  switch ($key) {
    case 'dedication-mail':
      $variables = array(
          '%site' => variable_get('site_name', 'drupal'),
          '%site_url' => $base_url,
          '%site_mail' => variable_get('site_mail', ini_get('send_mail_from')),
          '%card_url' => $params['card'],
          '%sender' => $params['sender'],
          '%sender_email' => $params['sendermail'],
          '%recipient' => $params['recipient'],
      );
      $body = strtr(variable_get('dedication_letter', _dedications_letter()), $variables);
      $subject = strtr(variable_get('dedication_subject', 'An dedication from %sender'), $variables);
      $message['subject'] = $subject;
      $message['body'] = $body;
      break;

    case 'dedication-copy':
      $variables = array(
          '%site' => variable_get('site_name', 'drupal'),
          '%site_url' => $base_url,
          '%site_mail' => variable_get('site_mail', ini_get('send_mail_from')),
          '%card_url' => $params['card_copy'],
      );
      $body = strtr(variable_get('dedication_copy', _dedications_copy()), $variables);
      $subject = strtr(variable_get('dedication_copy_subject', 'Copy of your dedication'), $variables);
      $message['subject'] = $subject;
      $message['body'] = $body;
      break;

    case 'dedication-notify':
      $variables = array(
          '%site' => variable_get('site_name', 'drupal'),
          '%site_url' => $base_url,
          '%site_mail' => variable_get('site_mail', ini_get('send_mail_from')),
          '%card_url' => $params['card_url'],
          '%sender' => $params['sender'],
          '%recipient' => $params['recipient'],
          '%sender_email' => $params['sender_email'],
      );
      $body = strtr(variable_get('dedication_notify', _dedications_notify()), $variables);
      $subject = strtr(variable_get('dedication_notify_subject', 'Your dedication has been picked up by %recipient'), $variables);
      $message['subject'] = $subject;
      $message['body'] = $body;
      break;
  }
}

/**
 * Function for the thank-you page.
 */
function dedications_thanks() {
  return t('Thanks, and have a nice day.');
}

/**
 * Implementation of hook_theme().
 */
function dedications_theme() {
  return array(
  'dedications_message' => array(
  'arguments' => array('message'),
  ),
  );
}

/**
 * Theme function to display dedication message.
 */
function theme_dedications_message($message) {
  return theme('fieldset',
  array(
  '#title' => t('Message'),
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
  '#value' => $message,
  )
  );
}

